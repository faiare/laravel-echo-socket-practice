<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/public-event', function () {
    broadcast(new \App\Events\PublicEvent);
    return 'public';
});

Route::get('/private-event', function () {
    $user = App\User::find(1);
    broadcast(new \App\Events\PrivateEvent($user));
    return 'private';
});
